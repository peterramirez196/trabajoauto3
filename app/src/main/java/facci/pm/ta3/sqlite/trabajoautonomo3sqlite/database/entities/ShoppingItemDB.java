package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;


import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;
import java.util.List;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }



    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Crea un nuevo mapa de valores, donde los nombres de columna son las claves
        ContentValues values = new ContentValues();
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName);
        // Insertar la nueva fila, devolviendo el valor de la clave primaria de la nueva fila
        long newRowId = db.insert(ShoppingElementEntry.TABLE_NAME, null, values);
    }

    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Ejecuta sentencia para eliminar todos los registros de la tabla
        db.execSQL("delete from "+ ShoppingElementEntry.TABLE_NAME);
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Nuevo nombre del producto
        String title = shoppingItem.getName();
        ContentValues values = new ContentValues();
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, title);
        // Especifica que fila se va a actualizar, basado en el id
        String selection = ShoppingElementEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(shoppingItem.getId()) }; // Id del producto
        // Ejecuta sentencia para actualizar el registro de la tabla
        int count = db.update(
                ShoppingElementEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Especifica el nomnre de la tabla a modificar
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?";
        // Especifica los argumentos en el orden de marcador de posición.
        String[] selectionArgs = { shoppingItem.getName() };
        // Ejecución de instrucción SQL.
        int deletedRows = db.delete(ShoppingElementEntry.TABLE_NAME, selection, selectionArgs);
    }
    public void buscar(ShoppingItem shoppingItem){
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                ShoppingElementEntry.COLUMN_NAME_TITLE

        };

        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE+ " = ?";
        String[] selectionArgs = { "My Title" };

        String sortOrder =
                ShoppingElementEntry.COLUMN_NAME_TITLE+ " DESC";

        Cursor cursor = db.query(
                ShoppingElementEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );
        List itemIds = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(

                    cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
            Log.e("aaaa", ShoppingElementEntry.COLUMN_NAME_TITLE);
            itemIds.add(itemId);
            Log.e("aaaa", ShoppingElementEntry.COLUMN_NAME_TITLE);
        }
        cursor.close();


    }
}
